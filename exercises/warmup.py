import pandas as pd

dic1 = {'Name':['Viper', 'Invoker', 'Luna', 'Riki', 'Rubick', 'Arc', 'Lina'],
        'Marks':[90,98,80,99,90,99,95],
        'Gender':['Male', 'Male', 'Female', 'Male', 'Male', 'Male', 'Female']
}
df1 = pd.DataFrame(dic1)
print(df1)

print("#########")
print("Display Top 3 Rows of Dataset")
print(df1.head(3))
print("#########")
print("Check last 3 rows of dataset")
print(df1.tail(3))

